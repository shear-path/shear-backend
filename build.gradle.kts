plugins {
    id("org.jetbrains.kotlin.jvm") version "1.4.10"
    id("org.jetbrains.kotlin.kapt") version "1.4.10"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.4.10"
    id("com.github.johnrengelman.shadow") version "6.1.0"
    id("io.micronaut.application") version "1.2.0"
}

version = "0.1"
group = "com.shear"


val kotlinVersion=project.properties.get("kotlinVersion")
val testcontianerVersion = project.properties.get("testcontainersVersion")
val junitVersion = project.properties.get("junitJupiterVersion")

repositories {
    mavenCentral()
    jcenter()
}

micronaut {
    runtime("netty")
    testRuntime("junit5")
    processing {
        incremental(true)
        annotations("com.shear.*")
    }
}

dependencies {
    kapt ("org.mapstruct:mapstruct-processor:1.4.1.Final")
    kapt ("io.micronaut:micronaut-validation")
    kapt ("io.micronaut:micronaut-inject")
    kapt ("io.micronaut.openapi:micronaut-openapi:2.3.0")
    implementation ("io.micronaut:micronaut-validation")
    implementation ("io.micronaut:micronaut-inject")
    implementation ("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${kotlinVersion}")
    implementation ("org.jetbrains.kotlin:kotlin-reflect:${kotlinVersion}")
    implementation ("io.micronaut.kotlin:micronaut-kotlin-runtime")
    implementation ("io.micronaut:micronaut-runtime")
    implementation ("javax.annotation:javax.annotation-api")
    implementation ("io.micronaut:micronaut-http-client")
    implementation ("org.mapstruct:mapstruct:1.4.1.Final")
    implementation(platform("software.amazon.awssdk:bom:2.15.50"))
    implementation ("software.amazon.awssdk:s3")
    implementation ("software.amazon.awssdk:dynamodb")
    implementation ("software.amazon.awssdk:dynamodb-enhanced")
    implementation("software.amazon.awssdk:cognitoidentityprovider")
    implementation ("io.swagger.core.v3:swagger-annotations")
    runtimeOnly ("ch.qos.logback:logback-classic")
    runtimeOnly ("com.fasterxml.jackson.module:jackson-module-kotlin")

    kaptTest ("io.micronaut:micronaut-inject-java")
    testImplementation("org.testcontainers:testcontainers:${testcontianerVersion}")
    testImplementation("org.testcontainers:junit-jupiter:${testcontianerVersion}")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params:${junitVersion}")
    testImplementation("io.micronaut.test:micronaut-test-junit5:2.3.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${junitVersion}")

}


application {
    mainClass.set("com.shear.Application")
}

java {
    sourceCompatibility = JavaVersion.toVersion("11")
}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = "11"
        }
    }
    compileTestKotlin {
        kotlinOptions {
            jvmTarget = "11"
        }
    }


}

kapt {
    arguments {
        arg("micronaut.openapi.views.spec", "redoc.enabled=true,rapidoc.enabled=true,swagger-ui.enabled=true,swagger-ui.theme=flattop")
    }
}

