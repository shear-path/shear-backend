package com.shear.core

import com.shear.core.entity.Employee
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.testcontainers.containers.GenericContainer
import org.testcontainers.junit.jupiter.Container
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.enhanced.dynamodb.Expression
import software.amazon.awssdk.enhanced.dynamodb.Key
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.*
import java.net.URI
import java.util.*

class KGenericContainer(imageName: String) : GenericContainer<KGenericContainer>(imageName)

@MicronautTest
class RepositoryTest {


    inner class EmployeeRepository : AbstractDynamodbRepository<Employee>(
        "employee", Employee::class.java, Region.US_EAST_2, client
    )

    private val employeeRepository = EmployeeRepository()

    companion object {

        private val logger = LoggerFactory.getLogger(RepositoryTest::class.java)

        @Container
        private val dynamodb = KGenericContainer("amazon/dynamodb-local:1.13.6")
            .withExposedPorts(8000)

        init {
            dynamodb.start()
        }

        private val client = DynamoDbClient.builder()
            .region(Region.US_EAST_2)
            .endpointOverride(URI.create("http://${dynamodb.containerIpAddress}:${dynamodb.getMappedPort(8000)}"))
            .credentialsProvider(
                StaticCredentialsProvider.create(
                    AwsBasicCredentials.create("access", "secret")
                )
            )
            .build()

        @BeforeAll
        @JvmStatic
        fun setup() {
            val tableResponse = client.createTable(
                CreateTableRequest.builder()
                    .tableName("employee")
                    .attributeDefinitions(
                        AttributeDefinition.builder()
                            .attributeName("PK").attributeType("S").build(),
                        AttributeDefinition.builder()
                            .attributeName("SK").attributeType("S").build(),
                        AttributeDefinition.builder()
                            .attributeName("GSI1PK").attributeType("S").build(),
                        AttributeDefinition.builder()
                            .attributeName("GSI1SK").attributeType("S").build()
                    )
                    .keySchema(
                        KeySchemaElement.builder().attributeName("PK").keyType(KeyType.HASH).build(),
                        KeySchemaElement.builder().attributeName("SK").keyType(KeyType.RANGE).build()
                    )
                    .provisionedThroughput(
                        ProvisionedThroughput.builder().readCapacityUnits(5).writeCapacityUnits(5).build()
                    )
                    .globalSecondaryIndexes(
                        GlobalSecondaryIndex.builder()
                            .indexName("employeeGroup")
                            .keySchema(
                                KeySchemaElement.builder().attributeName("GSI1PK").keyType(KeyType.HASH).build(),
                                KeySchemaElement.builder().attributeName("GSI1SK").keyType(KeyType.RANGE).build()
                            )
                            .projection(Projection.builder().projectionType(ProjectionType.ALL).build())
                            .provisionedThroughput(
                                ProvisionedThroughput.builder().readCapacityUnits(5).writeCapacityUnits(5).build()
                            )
                            .build()
                    )
                    .build()
            )

            logger.info("Table response : ${tableResponse.tableDescription().tableStatusAsString()}")
        }
    }


    @Test
    fun testSaveItem() {
        val pk = "EMPLOYEE#Put"
        val sk = "employee"

        val employee = Employee()
        employee.PK = pk
        employee.SK = sk
        employee.employeeId = 1
        employee.id = UUID.randomUUID()
        employee.firstName = "First Name"
        employee.lastName = "Last Name"

        val savedEmployee = employeeRepository.saveItem(employee)

        val expectedValues = client.getItem(
            GetItemRequest.builder().tableName(employeeRepository.tableName)
                .key(
                    mapOf(
                        "PK" to AttributeValue.builder().s(pk).build(),
                        "SK" to AttributeValue.builder().s(sk).build()
                    )
                ).build()
        ).item()

        assertEquals(expectedValues["PK"]?.s(), savedEmployee.PK)
        assertEquals(expectedValues["SK"]?.s(), savedEmployee.SK)
        assertEquals(expectedValues["EmployeeId"]?.n()?.toInt(), savedEmployee.employeeId)
        assertEquals(expectedValues["Id"]?.s(), savedEmployee.id.toString())
        assertEquals(expectedValues["FirstName"]?.s(), savedEmployee.firstName)
        assertEquals(expectedValues["LastName"]?.s(), savedEmployee.lastName)

    }

    @Test
    fun testGetItem() {

        val pk = "EMPLOYEE#Get"
        val sk = "employee"

        val itemValues = mutableMapOf(
            "EmployeeId" to AttributeValue.builder().n("2").build(),
            "Id" to AttributeValue.builder().s(UUID.randomUUID().toString()).build(),
            "FirstName" to AttributeValue.builder().s("Carles").build(),
            "LastName" to AttributeValue.builder().s("Puyol").build(),
            "PK" to AttributeValue.builder().s(pk).build(),
            "SK" to AttributeValue.builder().s(sk).build()
        )

        client.putItem(
            PutItemRequest.builder()
                .tableName(employeeRepository.tableName)
                .item(itemValues)
                .build()
        )

        val key = Key.builder()
            .partitionValue(pk)
            .sortValue(sk)
            .build()

        val actualEmployee = employeeRepository.getItem(key)

        assertEquals(itemValues["PK"]?.s(), actualEmployee?.PK)
        assertEquals(itemValues["SK"]?.s(), actualEmployee?.SK)
        assertEquals(itemValues["EmployeeId"]?.n()?.toInt(), actualEmployee?.employeeId)
        assertEquals(itemValues["Id"]?.s(), actualEmployee?.id.toString())
        assertEquals(itemValues["FirstName"]?.s(), actualEmployee?.firstName)
        assertEquals(itemValues["LastName"]?.s(), actualEmployee?.lastName)
    }

    @Test
    fun testDeleteItem() {
        val pk = "EMPLOYEE#Delete"
        val sk = "employee"

        val itemValues = mutableMapOf(
            "EmployeeId" to AttributeValue.builder().n("3").build(),
            "Id" to AttributeValue.builder().s(UUID.randomUUID().toString()).build(),
            "FirstName" to AttributeValue.builder().s("First").build(),
            "LastName" to AttributeValue.builder().s("Last").build(),
            "PK" to AttributeValue.builder().s(pk).build(),
            "SK" to AttributeValue.builder().s(sk).build()
        )

        client.putItem(
            PutItemRequest.builder()
                .tableName("employee")
                .item(itemValues)
                .build()
        )

        val key = Key.builder()
            .partitionValue(pk)
            .sortValue(sk)
            .build()

        employeeRepository.deleteItem(key)

        val response = client.getItem(
            GetItemRequest.builder().tableName(employeeRepository.tableName)
                .key(
                    mapOf(
                        "PK" to AttributeValue.builder().s(pk).build(),
                        "SK" to AttributeValue.builder().s(sk).build()
                    )
                ).build()
        )

        assertFalse(response.hasItem())
    }


    @Test
    fun testUpdateItem() {

        val pk = "EMPLOYEE#Update"
        val sk = "employee"


        client.putItem(
            PutItemRequest.builder()
                .tableName(employeeRepository.tableName)
                .item(
                    mutableMapOf(
                        "EmployeeId" to AttributeValue.builder().n("4").build(),
                        "Id" to AttributeValue.builder().s(UUID.randomUUID().toString()).build(),
                        "FirstName" to AttributeValue.builder().s("First").build(),
                        "LastName" to AttributeValue.builder().s("Last").build(),
                        "PK" to AttributeValue.builder().s(pk).build(),
                        "SK" to AttributeValue.builder().s(sk).build()
                    )
                )
                .build()
        )

        val employee = Employee()
        employee.PK = pk
        employee.SK = sk
        employee.employeeId = 5
        employee.id = UUID.randomUUID()
        employee.firstName = "First Updated"
        employee.lastName = "Last Updated"

        val key = Key.builder()
            .partitionValue(pk)
            .sortValue(sk)
            .build()

        val updated = employeeRepository.updateItem(key, employee)

        val itemValues = client.getItem(
            GetItemRequest.builder().tableName(employeeRepository.tableName)
                .key(
                    mapOf(
                        "PK" to AttributeValue.builder().s(pk).build(),
                        "SK" to AttributeValue.builder().s(sk).build()
                    )
                ).build()
        ).item()

        assertEquals(itemValues["PK"]?.s(), updated?.PK)
        assertEquals(itemValues["SK"]?.s(), updated?.SK)
        assertEquals(itemValues["EmployeeId"]?.n()?.toInt(), updated?.employeeId)
        assertEquals(itemValues["Id"]?.s(), updated?.id.toString())
        assertEquals(itemValues["FirstName"]?.s(), updated?.firstName)
        assertEquals(itemValues["LastName"]?.s(), updated?.lastName)
    }

    @Test
    fun testGetItems() {

        val pk = "MANAGER#"
        val skBegin = "manager"

        val items = listOf(
            WriteRequest.builder().putRequest(
                PutRequest.builder()
                    .item(
                        mutableMapOf(
                            "EmployeeId" to AttributeValue.builder().n("5").build(),
                            "Id" to AttributeValue.builder().s(UUID.randomUUID().toString()).build(),
                            "FirstName" to AttributeValue.builder().s("ItemOneFirst").build(),
                            "LastName" to AttributeValue.builder().s("ItemOneLast").build(),
                            "Type" to AttributeValue.builder().s("MANAGER").build(),
                            "PK" to AttributeValue.builder().s(pk).build(),
                            "SK" to AttributeValue.builder().s("${skBegin}1").build()
                        )
                    ).build()
            ).build(),
            WriteRequest.builder().putRequest(
                PutRequest.builder()
                    .item(
                        mutableMapOf(
                            "EmployeeId" to AttributeValue.builder().n("6").build(),
                            "Id" to AttributeValue.builder().s(UUID.randomUUID().toString()).build(),
                            "FirstName" to AttributeValue.builder().s("ItemTwoFirst").build(),
                            "LastName" to AttributeValue.builder().s("ItemTwoLast").build(),
                            "Type" to AttributeValue.builder().s("MANAGER").build(),
                            "PK" to AttributeValue.builder().s(pk).build(),
                            "SK" to AttributeValue.builder().s("${skBegin}2").build()
                        )
                    ).build()
            ).build()
        )



        client.batchWriteItem(
            BatchWriteItemRequest.builder()
                .requestItems(mapOf(employeeRepository.tableName to items))
                .build()
        )

        val conditional = QueryEnhancedRequest.builder()
            .queryConditional(
                QueryConditional.sortBeginsWith(
                    Key.builder()
                        .partitionValue(pk)
                        .sortValue(skBegin)
                        .build()
                )
            )
            .filterExpression(
                Expression.builder()
                    .expression("#type = :type")
                    .expressionNames(mutableMapOf("#type" to "Type"))
                    .expressionValues(mutableMapOf(":type" to AttributeValue.builder().s("MANAGER").build()))
                    .build()
            )
            .build()

        val pages = employeeRepository.getItems(conditional)
            .toList()

        val list = mutableListOf<Employee>()

        for (page in pages)
            list.addAll(page.items())

        list.forEach { logger.info(it.toString()) }

        assertEquals(2, list.size)
    }

    @Test
    fun testGetItemsWithIndex() {

        val gsi1PK = "marketing"

        val items = listOf(
            WriteRequest.builder().putRequest(
                PutRequest.builder()
                    .item(
                        mutableMapOf(
                            "EmployeeId" to AttributeValue.builder().n("5").build(),
                            "Id" to AttributeValue.builder().s(UUID.randomUUID().toString()).build(),
                            "FirstName" to AttributeValue.builder().s("ItemOneFirst").build(),
                            "LastName" to AttributeValue.builder().s("ItemOneLast").build(),
                            "Type" to AttributeValue.builder().s("MANAGER").build(),
                            "PK" to AttributeValue.builder().s("EMPLOYEE#GSI11").build(),
                            "SK" to AttributeValue.builder().s("employee").build(),
                            "GSI1PK" to AttributeValue.builder().s(gsi1PK).build(),
                            "GSI1SK" to AttributeValue.builder().s("EMPLOYEE#GSI11").build()
                        )
                    ).build()
            ).build(),
            WriteRequest.builder().putRequest(
                PutRequest.builder()
                    .item(
                        mutableMapOf(
                            "EmployeeId" to AttributeValue.builder().n("6").build(),
                            "Id" to AttributeValue.builder().s(UUID.randomUUID().toString()).build(),
                            "FirstName" to AttributeValue.builder().s("ItemTwoFirst").build(),
                            "LastName" to AttributeValue.builder().s("ItemTwoLast").build(),
                            "Type" to AttributeValue.builder().s("MANAGER").build(),
                            "PK" to AttributeValue.builder().s("EMPLOYEE#GSI12").build(),
                            "SK" to AttributeValue.builder().s("employee").build(),
                            "GSI1PK" to AttributeValue.builder().s(gsi1PK).build(),
                            "GSI1SK" to AttributeValue.builder().s("EMPLOYEE#GSI12").build()
                        )
                    ).build()
            ).build()
        )

        client.batchWriteItem(
            BatchWriteItemRequest.builder()
                .requestItems(mapOf(employeeRepository.tableName to items))
                .build()
        )

        val conditional = QueryEnhancedRequest.builder()
            .queryConditional(
                QueryConditional.keyEqualTo(
                    Key.builder()
                        .partitionValue(gsi1PK)
                        .build()
                )
            )
            .build()

        val groupIndex = employeeRepository.mappedTable.index("employeeGroup")

        val pages = employeeRepository.getItems(groupIndex, conditional).toList()

        val list = mutableListOf<Employee>()

        for (page in pages)
            list.addAll(page.items())

        list.forEach { logger.info(it.toString()) }

        assertEquals(2, list.size)
    }
}


