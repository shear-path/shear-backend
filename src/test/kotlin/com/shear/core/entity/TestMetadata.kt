package com.shear.core.entity

import com.shear.core.AbstractDynamodbRepository
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*
import java.util.*
import javax.inject.Singleton
import kotlin.properties.Delegates


@DynamoDbBean
class Employee {

    var type = "EMPLOYEE"
        @DynamoDbAttribute("Type") get

    lateinit var PK :String
        @DynamoDbPartitionKey get

    lateinit var SK : String
        @DynamoDbSortKey get

    lateinit var id : UUID
        @DynamoDbAttribute("Id") get

    lateinit var firstName : String
        @DynamoDbAttribute("FirstName") get

    lateinit var lastName : String
        @DynamoDbAttribute("LastName") get

    var employeeId by Delegates.notNull<Int>()
        @DynamoDbAttribute("EmployeeId") get

    var GSI1PK : String? = null
        @DynamoDbSecondaryPartitionKey(indexNames = ["employeeGroup"]) get

    var GSI1SK : String?  = null
        @DynamoDbSecondarySortKey(indexNames = ["employeeGroup"]) get

    override fun toString() : String {
        return """
            [
                PK: $PK
                SK: $SK
                Id: $id
                First Name: $firstName
                Last Name: $lastName
                Employee Id: $employeeId
                GSI1PK: $GSI1PK
                GSI1SK: $GSI1SK
            ]
        """.trimIndent()
    }
}