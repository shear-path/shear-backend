package com.shear.booking.dto

import com.shear.booking.entity.BookingEvent
import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.mapstruct.NullValuePropertyMappingStrategy
import java.math.BigInteger
import java.time.Instant
import java.util.*


enum class BookingStatus {
    ACCEPTED,
    CANCELLED,
    PENDING
}

data class BookingDTO (
    var id : UUID?,
    val serviceName : String,
    val providerId : UUID,
    var organizationId : UUID?,
    val clientId: UUID,
    val clientName: String,
    val providerName: String,
    val dateTimeStart: Instant,
    val duration : Int,
    var notes : String?,
    val price : BigInteger,
    val status : BookingStatus = BookingStatus.PENDING
)

data class UpdateBookingDTO(
    val status : BookingStatus
)

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, componentModel = "jsr330")
interface BookingMapper {
    fun map(dto : BookingDTO) : BookingEvent

    fun map(entity : BookingEvent?) : BookingDTO

    fun map(dto : UpdateBookingDTO, @MappingTarget entity : BookingEvent?)
}