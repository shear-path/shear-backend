package com.shear.booking.controller

import com.shear.booking.dto.BookingDTO
import com.shear.booking.dto.UpdateBookingDTO
import com.shear.booking.service.BookingService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.*
import java.util.*
import javax.inject.Inject
import javax.validation.Valid

@Controller("/bookings")
class BookingController (@Inject private val service: BookingService){

    @Post
    fun createBooking(@Body dto : BookingDTO) = service.createBooking(dto)

    @Get("/{bookingId}")
    fun getBooking(bookingId : UUID) = service.getBooking(bookingId)


    @Get("/{parameters}")
    fun getEvents(@Valid parameters: SearchParameters ) : HttpResponse<List<BookingDTO>>{

        if(parameters.clientId != null)
            return HttpResponse.ok(service.getClientBookings(parameters.clientId!!))
        else if(parameters.providerId != null)
            return HttpResponse.ok(service.getProviderBookings(parameters.providerId!!))

        return HttpResponse.badRequest()
    }


    @Get("/providers/{providerId}")
    fun getProviderBookings(providerId : UUID) = service.getProviderBookings(providerId)

    @Get("/clients/{clientId}")
    fun getClientBookings(clientId : UUID) = service.getClientBookings(clientId)

    fun getOrganizationBookings(){
        TODO("Not yet implemented")
    }

    @Patch("/{bookingId}")
    fun updateBooking(bookingId: UUID, @Body dto : UpdateBookingDTO) = service.updateBooking(bookingId, dto)


}

class SearchParameters() {


    var clientId: UUID? = null

    var providerId: UUID? = null
}

enum class EventType {
    BOOKING,
    BUSY
}