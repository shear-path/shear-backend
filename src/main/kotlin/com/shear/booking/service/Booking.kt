package com.shear.booking.service

import com.shear.booking.dto.BookingDTO
import com.shear.booking.dto.BookingMapper
import com.shear.booking.dto.UpdateBookingDTO
import com.shear.booking.entity.BookingEvent
import com.shear.core.AbstractDynamodbRepository
import software.amazon.awssdk.enhanced.dynamodb.Expression
import software.amazon.awssdk.enhanced.dynamodb.Key
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

interface BookingService {
    fun getBooking(bookingId: UUID): BookingDTO

    fun createBooking(dto: BookingDTO): BookingDTO

    fun getProviderBookings(providerId: UUID): List<BookingDTO>

    fun getClientBookings(clientId: UUID): List<BookingDTO>

    fun getOrganizationBookings(organizationId: UUID): List<BookingDTO>

    fun updateBooking(bookingId: UUID, dto: UpdateBookingDTO): BookingDTO?
}

class BookingRepository : AbstractDynamodbRepository<BookingEvent>(
    "provider", BookingEvent::class.java
)

@Singleton
class BookingServiceImpl(
    @Inject private val mapper: BookingMapper
) : BookingService {

    private val repository = BookingRepository()

    private val providerBookingsIndex = repository.mappedTable.index("providerBookings")

    private val clientBookingsIndex = repository.mappedTable.index("clientBookings")


    override fun getBooking(bookingId: UUID): BookingDTO {
        val key = Key.builder()
            .partitionValue("BOOKING#$bookingId")
            .sortValue("DETAILS#$bookingId")
            .build()

        return mapper.map(repository.getItem(key))
    }

    override fun createBooking(dto: BookingDTO): BookingDTO {
        val booking = mapper.map(dto)
        booking.id = UUID.randomUUID()
        booking.PK = "${booking.type}#${booking.id}"
        booking.SK = "DETAILS#${booking.id}"

        booking.GSI2PK = "PROVIDER#${booking.providerId}#BOOKING"
        booking.GSI2SK = booking.dateTimeStart.toEpochMilli()

        booking.GSI3PK = "CLIENT#${booking.clientId}#BOOKING"
        booking.GSI3SK = booking.dateTimeStart.toEpochMilli()

        if (booking.organizationId != null) {
            booking.GSI4PK = "ORGANIZATION#${booking.organizationId}#BOOKING"
            booking.GSI4SK = booking.dateTimeStart.toEpochMilli()
        }

        return mapper.map(repository.saveItem(booking))
    }

    override fun getProviderBookings(providerId: UUID): List<BookingDTO> {
        val partition = "PROVIDER#$providerId#BOOKING"

        val pages = repository.getItems(providerBookingsIndex, bookingFilterQuery(partition)).toList()

        val list = mutableListOf<BookingDTO>()

        for (page in pages)
            list.addAll(page.items().map { mapper.map(it) })

        return list
    }

    override fun getClientBookings(clientId: UUID): List<BookingDTO> {
        val partition = "CLIENT#$clientId#BOOKING"

        val pages = repository.getItems(clientBookingsIndex, bookingFilterQuery(partition)).toList()

        val list = mutableListOf<BookingDTO>()

        for (page in pages)
            list.addAll(page.items().map { mapper.map(it) })

        return list
    }

    override fun getOrganizationBookings(organizationId: UUID): List<BookingDTO> {
        TODO("Not yet implemented")
    }

    override fun updateBooking(bookingId: UUID, dto: UpdateBookingDTO): BookingDTO? {
        val key = Key.builder()
            .partitionValue("BOOKING#$bookingId")
            .sortValue("DETAILS#$bookingId")
            .build()

        val booking = repository.getItem(key)

        mapper.map(dto, booking)

        val updated = booking?.let { repository.updateItem(key, it) }
        return mapper.map(updated)
    }

    private fun bookingFilterQuery(partitionKey: String): QueryEnhancedRequest {
        val key = Key.builder()
            .partitionValue(partitionKey)
            .build()

        return QueryEnhancedRequest.builder()
            .queryConditional(QueryConditional.keyEqualTo(key))
            .filterExpression(
                Expression.builder()
                    .expression("#type = :type")
                    .expressionNames(mutableMapOf("#type" to "Type"))
                    .expressionValues(mutableMapOf(":type" to AttributeValue.builder().s("BOOKING").build()))
                    .build()
            )
            .build()
    }

}