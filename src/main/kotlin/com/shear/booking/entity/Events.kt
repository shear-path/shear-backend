package com.shear.booking.entity

import com.shear.booking.dto.BookingStatus
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*
import java.math.BigInteger
import java.time.Instant
import java.util.*
import kotlin.properties.Delegates




@DynamoDbBean
class BookingEvent()  {

    var type = "BOOKING"
        @DynamoDbAttribute("Type") get

    lateinit var PK :String
        @DynamoDbPartitionKey get

    lateinit var SK : String
        @DynamoDbSortKey get

    var  id : UUID? = null
        @DynamoDbAttribute("Id") get

    lateinit var serviceName: String
        @DynamoDbAttribute("ServiceName") get

    lateinit var providerId: UUID
        @DynamoDbAttribute("ProviderId") get

    var organizationId: UUID? = null
        @DynamoDbAttribute("OrganizationId") get

    lateinit var clientId: UUID
        @DynamoDbAttribute("ClientId") get

    lateinit var clientName: String
        @DynamoDbAttribute("ClientName") get

    lateinit var providerName: String
        @DynamoDbAttribute("ProviderName") get

    lateinit var dateTimeStart: Instant
        @DynamoDbAttribute("DateTimeStart") get

    var duration by Delegates.notNull<Int>()
        @DynamoDbAttribute("Duration")get

    var notes: String? = null
        @DynamoDbAttribute("Notes") get

    lateinit var price: BigInteger
        @DynamoDbAttribute("Price") get

    lateinit var status: BookingStatus
        @DynamoDbAttribute("Status") get

    //Booking to Provider Index
    lateinit var GSI2PK : String
        @DynamoDbSecondaryPartitionKey(indexNames = ["providerBookings"]) get

    var GSI2SK by Delegates.notNull<Long>()
        @DynamoDbSecondarySortKey(indexNames = ["providerBookings"]) get

    //Booking to Client Index
    lateinit var GSI3PK : String
        @DynamoDbSecondaryPartitionKey(indexNames = ["clientBookings"]) get

    var GSI3SK by Delegates.notNull<Long>()
        @DynamoDbSecondarySortKey(indexNames = ["clientBookings"]) get

    //Booking to Organization Index
     var GSI4PK :String? = null
        @DynamoDbSecondaryPartitionKey(indexNames = ["organizationBookings"]) get

    var GSI4SK by Delegates.notNull<Long>()
        @DynamoDbSecondarySortKey(indexNames = ["organizationBookings"]) get

}

@DynamoDbBean
class BusyEvent(){

}