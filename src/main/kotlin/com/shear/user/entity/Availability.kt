package com.shear.user.entity

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbAttribute
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean
import java.time.DayOfWeek
import java.time.Instant
import java.time.LocalTime
import java.util.*

@DynamoDbBean
class Availability () {

    var type = "AVAILABILITY"
        @DynamoDbAttribute("Type") get

    lateinit var id : UUID
        @DynamoDbAttribute("Id") get

    lateinit var providerId : UUID
        @DynamoDbAttribute("ProviderId") get

    lateinit var timeStart : String
        @DynamoDbAttribute("TimeStart") get

    lateinit var timeEnd : String
        @DynamoDbAttribute("TimeEnd") get

    lateinit var dayOfWeek: String
        @DynamoDbAttribute("DayOfWeek") get
}