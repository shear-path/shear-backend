package com.shear.user.entity

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*
import java.util.*
import kotlin.collections.ArrayList

@DynamoDbBean
class Organization{

    var type = "ORGANIZATION"
        @DynamoDbAttribute("Type") get

    lateinit var PK :String
        @DynamoDbPartitionKey get

    lateinit var SK : String
        @DynamoDbSortKey get

    lateinit var name : String
        @DynamoDbAttribute("Name") get

    lateinit var id : UUID
        @DynamoDbAttribute("Id") get

    var locations : MutableList<Location> = ArrayList<Location>()

    var GSI1PK : String? = null
        @DynamoDbSecondaryPartitionKey(indexNames = ["GSI1"]) get

    var GSI1SK : String?  = null
        @DynamoDbSecondarySortKey(indexNames = ["GSI1"]) get

}

@DynamoDbBean
class Location {

    lateinit var address1: String
    var address2: String? = null
    lateinit var city: String
    lateinit var state: String
    lateinit var country: String
    lateinit var zipcode: String

    constructor()

    constructor(address1: String, address2: String, city: String, state: String, country: String, zipcode: String) {
        this.address1 = address1
        this.address2 = address2
        this.city = city
        this.state = state
        this.country = country
        this.zipcode = zipcode
    }


}

@DynamoDbBean
class Admin {
    var type = "ADMINISTRATOR"
        @DynamoDbAttribute("Type") get

    lateinit var PK :String
        @DynamoDbPartitionKey get

    lateinit var SK : String
        @DynamoDbSortKey get

    lateinit var  id : UUID
        @DynamoDbAttribute("Id") get

    var firstName: String? = null
        @DynamoDbAttribute("FirstName") get

    var lastName: String? = null
        @DynamoDbAttribute("LastName") get

    var email : String? = null
        @DynamoDbAttribute("Email") get
}