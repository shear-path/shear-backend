package com.shear.user.entity

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*
import java.math.BigInteger
import java.util.*
import kotlin.collections.ArrayList


@DynamoDbBean
class Provider {

    var type = "PROVIDER"
        @DynamoDbAttribute("Type") get

    lateinit var PK :String
        @DynamoDbPartitionKey get

    lateinit var SK : String
        @DynamoDbSortKey get


    lateinit var  id : UUID
        @DynamoDbAttribute("Id") get

    var firstName: String? = null
        @DynamoDbAttribute("FirstName") get

    var lastName: String? = null
        @DynamoDbAttribute("LastName") get

    var email : String? = null
        @DynamoDbAttribute("Email") get

    var services : MutableList<Service>  = ArrayList<Service>()
        @DynamoDbAttribute("Services") get

    var categories : MutableList<String>  = ArrayList<String>()
        @DynamoDbAttribute("Categories") get

    var images : MutableList<String> = ArrayList<String>()
        @DynamoDbAttribute("Images") get

    var organizationId: UUID? = null
        @DynamoDbAttribute("OrganizationId") get

    var GSI1PK : String? = null
        @DynamoDbSecondaryPartitionKey(indexNames = ["GSI1"]) get

    var GSI1SK : String?  = null
        @DynamoDbSecondarySortKey(indexNames = ["GSI1"]) get

    constructor()

    constructor(id : UUID, firstName: String, lastName : String, email: String){
        this.id = id
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
    }
}

@DynamoDbBean
class Service{

    lateinit var name: String

    lateinit var  price: BigInteger

    constructor()

    constructor(name: String, price : BigInteger){
        this.name = name
        this.price = price
    }
}

@DynamoDbBean
class Client {

    var type = "CLIENT"
        @DynamoDbAttribute("Type") get

    lateinit var PK :String
        @DynamoDbPartitionKey get

    lateinit var SK : String
        @DynamoDbSortKey get


    lateinit var  id : UUID
        @DynamoDbAttribute("Id") get

    var firstName: String? = null
        @DynamoDbAttribute("FirstName") get

    var lastName: String? = null
        @DynamoDbAttribute("LastName") get

    var email : String? = null
        @DynamoDbAttribute("Email") get
}

@DynamoDbBean
class Membership (){

    var type = "MEMBERSHIP"
        @DynamoDbAttribute("Type") get

    lateinit var  providerId : UUID
        @DynamoDbAttribute("ProviderId") get

    var firstName: String? = null
        @DynamoDbAttribute("FirstName") get

    var lastName: String? = null
        @DynamoDbAttribute("LastName") get
}