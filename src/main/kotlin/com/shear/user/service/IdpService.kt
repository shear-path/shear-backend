package com.shear.user.service

import com.shear.user.dto.UserDTO
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminCreateUserRequest
import software.amazon.awssdk.services.cognitoidentityprovider.model.AttributeType
import java.util.*
import javax.inject.Singleton

interface IdpService {

    fun createUser(user: UserDTO): UserDTO
}

@Singleton
class CognitoIdpService : IdpService {

    private val cognitoClient = CognitoIdentityProviderClient.builder()
        .region(Region.US_EAST_2)
        .build()

    private val userPoolId = "us-east-2_QORLyhRci"

    override fun createUser(user: UserDTO): UserDTO {

        val response = cognitoClient.adminCreateUser(
            AdminCreateUserRequest.builder()
                .userPoolId(userPoolId)
                .username(user.username)
                .userAttributes(AttributeType.builder().name("nickname").value(user.lastName).build(),
                    AttributeType.builder().name("email").value(user.email).build(),
                    AttributeType.builder().name("name").value(user.lastName).build(),
                    AttributeType.builder().name("family_name").value(user.firstName).build(),
                    AttributeType.builder().name("custom:organizationId").value(user.organizationId.toString()).build())
                .build()
        )


        for (type in response.user().attributes()) {
            if (type.name() == "sub") {
                user.id = UUID.fromString(type.value())
                break
            }
        }

        return user
    }


}