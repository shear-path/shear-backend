package com.shear.user.service

import com.shear.core.AbstractDynamodbRepository
import com.shear.user.dto.CreateOrganizationDTO
import com.shear.user.dto.Mapper
import com.shear.user.dto.OrganizationDTO
import com.shear.user.dto.UserDTO
import com.shear.user.entity.Admin
import com.shear.user.entity.Organization
import org.slf4j.LoggerFactory
import software.amazon.awssdk.enhanced.dynamodb.Key
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

interface OrganizationService{

    fun createOrganization(dto: CreateOrganizationDTO): OrganizationDTO

    fun getOrganization(organizationId: UUID): OrganizationDTO?

    fun createOrgAdmin(dto : UserDTO, organizationId: UUID?) : UserDTO
}

class OrganizationRepository : AbstractDynamodbRepository<Organization>(
    "provider", Organization::class.java)

class AdminRepository : AbstractDynamodbRepository<Admin>(
    "provider", Admin::class.java
)



@Singleton
class OrganizationServiceImpl(
    @Inject private val mapper: Mapper
) : OrganizationService {


    private val logger = LoggerFactory.getLogger(OrganizationServiceImpl::class.java)

    private val organizationRepository  = OrganizationRepository()

    private val adminRepository = AdminRepository()

    override fun createOrganization(dto: CreateOrganizationDTO): OrganizationDTO {
        var organization = mapper.map(dto)

        val uuid = UUID.randomUUID()

        organization.PK = "${organization.type}#${uuid}"
        organization.SK = "METADATA#${uuid}"
        organization.id = uuid
        //TODO: review
        organization.GSI1PK = "${organization.type}#$uuid#PROVIDER"
        organization.GSI1SK = organization.PK

        organization = organizationRepository.saveItem(organization)

        val admin = createOrgAdmin(dto.admin, uuid)
        val org = mapper.map(organization)
        org.admin = admin

        return org
    }

    override fun getOrganization(organizationId: UUID): OrganizationDTO? {
        val key = Key.builder()
            .partitionValue("ORGANIZATION#$organizationId")
            .sortValue("METADATA#$organizationId")
            .build()

        val organization  = organizationRepository.getItem(key)

        return mapper.map(organization)
    }


    override fun createOrgAdmin(dto: UserDTO, organizationId: UUID?): UserDTO {
        var admin = mapper.mapAdmin(dto)

        admin.PK = "ORGANIZATION#$organizationId"
        admin.SK = "${admin.type}#${admin.id}"

        admin = adminRepository.saveItem(admin)

        return mapper.map(admin)
    }

}