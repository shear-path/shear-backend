package com.shear.user.service

import com.shear.core.AbstractDynamodbRepository
import com.shear.user.dto.Mapper
import com.shear.user.dto.ProviderDTO
import com.shear.user.dto.UpdateProviderDTO
import com.shear.user.dto.UserDTO
import com.shear.user.entity.Provider
import io.micronaut.http.multipart.CompletedFileUpload
import org.slf4j.LoggerFactory
import software.amazon.awssdk.enhanced.dynamodb.Expression
import software.amazon.awssdk.enhanced.dynamodb.Key
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

interface ProviderService {

    fun createProvider(dto: UserDTO): ProviderDTO?

    fun createProvider(dto: ProviderDTO): ProviderDTO?

    fun getProvider(providerId: UUID): ProviderDTO?

    fun deleteProvider(providerId: UUID)

    fun updateProvider(providerId: UUID, providerDTO: UpdateProviderDTO): ProviderDTO?

    fun uploadImage(file: CompletedFileUpload, providerId: UUID): Boolean

    fun deleteProviderImage(providerId: UUID, filename: String): Boolean

    fun getOrganizationProviders(organizationId: UUID): List<ProviderDTO?>
}



@Singleton
class ProviderServiceImpl(
    @Inject private val mapper: Mapper,
    @Inject private val fileService: FileService
) : ProviderService {


    inner class ProviderRepository : AbstractDynamodbRepository<Provider>("provider", Provider::class.java)

    private val logger = LoggerFactory.getLogger(ProviderServiceImpl::class.java)

    private val cloudfrontUri = "https://d2w6vfi1te1142.cloudfront.net"

    private val repository = ProviderRepository()

    private val organizationProviders = repository.mappedTable.index("GSI1")

    override fun createProvider(dto: UserDTO): ProviderDTO? {
        return createProvider(mapper.map(dto))
    }

    override fun createProvider(dto: ProviderDTO): ProviderDTO? {
        val provider = mapper.map(dto)
        provider.PK = "${provider.type}#${provider.id}"
        provider.SK = "PROFILE#${provider.id}"

        if (dto.organizationId != null) {
            provider.GSI1PK = "ORGANIZATION#${dto.organizationId}#${provider.type}"
            provider.GSI1SK = provider.PK
        }

        val saved = repository.saveItem(provider)
        return mapper.map(saved)
    }

    override fun getProvider(providerId: UUID): ProviderDTO? {
        val key = Key.builder()
            .partitionValue("PROVIDER#$providerId")
            .sortValue("PROFILE#$providerId")
            .build()

        val provider = repository.getItem(key)
        return mapper.map(provider)
    }

    override fun deleteProvider(providerId: UUID) {
        repository.deleteItem(
            Key
                .builder()
                .partitionValue("PROVIDER#$providerId")
                .sortValue("PROVIDER#$providerId")
                .build()
        )
    }

    override fun updateProvider(providerId: UUID, providerDTO: UpdateProviderDTO): ProviderDTO? {
        val key = Key.builder()
            .partitionValue("PROVIDER#$providerId")
            .sortValue("PROFILE#$providerId")
            .build()

        val provider = repository.getItem(key)

        mapper.map(providerDTO, provider)

        val updated = provider?.let { repository.updateItem(key, it) }
        return mapper.map(updated)
    }

    override fun uploadImage(file: CompletedFileUpload, providerId: UUID): Boolean {
        val key = Key.builder()
            .partitionValue("PROVIDER#$providerId")
            .sortValue("PROFILE#$providerId")
            .build()

        val provider = repository.getItem(key) ?: return false

        try {
            val fileName = "${provider.id}#${UUID.randomUUID()}"

            val tempFile = File.createTempFile(fileName, ".jpg")
            val path = Paths.get(tempFile.absolutePath)
            Files.write(path, file.bytes)

            fileService.uploadFile(tempFile)
            provider.images.add("$cloudfrontUri/${tempFile.name}")

            repository.updateItem(key, provider)
        } catch (e: Exception) {
            logger.error("Error while uploading file", e)
        }

        return true
    }

    override fun deleteProviderImage(providerId: UUID, filename: String): Boolean {
        val key = Key.builder()
            .partitionValue("PROVIDER#$providerId")
            .sortValue("PROFILE#$providerId")
            .build()

        val provider = repository.getItem(key) ?: return false

        provider.images.remove(filename)

        repository.updateItem(key, provider)

        return true
    }

    override fun getOrganizationProviders(organizationId: UUID): List<ProviderDTO?> {
        val key = Key.builder()
            .partitionValue("ORGANIZATION#$organizationId#PROVIDER")
            .build()

        val conditional = QueryEnhancedRequest.builder()
            .queryConditional(QueryConditional.keyEqualTo(key))
            .filterExpression(
                Expression.builder()
                    .expression("#type = :type")
                    .expressionNames(mutableMapOf("#type" to "Type"))
                    .expressionValues(mutableMapOf(":type" to AttributeValue.builder().s("PROVIDER").build()))
                    .build()
            )
            .build()

        val pages = repository.getItems(organizationProviders,conditional).toList()

        val list = mutableListOf<ProviderDTO?>()

        for (page in pages){
            list.addAll(page.items().map { mapper.map(it) })
        }


        return list
    }

}