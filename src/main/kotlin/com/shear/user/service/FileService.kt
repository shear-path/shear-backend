package com.shear.user.service

import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.PutObjectRequest
import java.io.File
import javax.inject.Singleton

interface FileService {

    fun uploadFile(file : File)

    fun deleteFile(fileName: String)
}

@Singleton
class FileServiceImpl : FileService {


    private val s3 = S3Client.builder().region(Region.US_EAST_2).build()

    private val bucketName = "aircuts-service-provider-images"

    override fun uploadFile(file: File) {
        val request = PutObjectRequest.builder()
            .bucket(bucketName)
            .key(file.name)
            .build()

        s3.putObject(request, RequestBody.fromFile(file))
    }

    override fun deleteFile(fileName: String) {
        TODO("Not yet implemented")
    }

}