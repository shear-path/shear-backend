package com.shear.user.service

import com.shear.core.AbstractDynamodbRepository
import com.shear.user.dto.Mapper
import com.shear.user.dto.UserDTO
import com.shear.user.entity.Client
import javax.inject.Inject

interface UserService {

    fun createClient(dto: UserDTO): UserDTO
}

class ClientRepository : AbstractDynamodbRepository<Client>(
    "provider", Client::class.java
)

class UserServiceImpl(
    @Inject private val mapper: Mapper
) : UserService {

    private val repository =  ClientRepository()

    override fun createClient(dto: UserDTO): UserDTO {
        val client = mapper.mapClient(dto)

        client.PK = "CLIENT#${client.id}"
        client.SK = "PROFILE#${client.id}"

        return mapper.map(repository.saveItem(client))
    }

}