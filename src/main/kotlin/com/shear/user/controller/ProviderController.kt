package com.shear.user.controller

import com.shear.user.dto.ProviderDTO
import com.shear.user.dto.UpdateProviderDTO
import com.shear.user.service.ProviderService
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import io.micronaut.http.multipart.CompletedFileUpload
import java.util.*
import javax.inject.Inject

@Controller("/providers")
class ProviderController(@Inject private val service: ProviderService) {

    @Post(produces = [MediaType.APPLICATION_JSON])
    fun createProvider(@Body providerDTO: ProviderDTO): HttpResponse<ProviderDTO?> =
        HttpResponse.created(service.createProvider(providerDTO))

    @Get(value = "/{providerId}", produces = [MediaType.APPLICATION_JSON])
    fun getProvider(providerId: UUID): HttpResponse<ProviderDTO?> {
        val dto = service.getProvider(providerId) ?: return HttpResponse.noContent()

        return HttpResponse.ok(dto)
    }

    fun getProviders() {
        TODO("Not yet implemented")
    }

    @Delete("/{providerId}")
    fun deleteProvider(providerId: UUID) {
        service.deleteProvider(providerId)
    }

    @Patch("/{providerId}")
    fun updateProvider(providerId: UUID, @Body dto: UpdateProviderDTO): HttpResponse<ProviderDTO?> {
        val provider = service.updateProvider(providerId, dto) ?: return HttpResponse.noContent()
        return HttpResponse.ok(provider)
    }

    @Post(
        value = "/{providerId}/images",
        consumes = [MediaType.MULTIPART_FORM_DATA],
        produces = [MediaType.TEXT_PLAIN]
    )
    fun uploadImage(file: CompletedFileUpload, providerId: UUID): HttpResponse<String> {
        return if (service.uploadImage(
                file,
                providerId
            )
        ) HttpResponse.ok("Uploaded") else HttpResponse.badRequest("Upload Failed")
    }

    fun uploadProfilePic() {
        TODO("not implemented")
    }

    fun deleteImage() {
        TODO("not implemented")
    }
}