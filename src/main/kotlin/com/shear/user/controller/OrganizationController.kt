package com.shear.user.controller

import com.shear.user.dto.CreateOrganizationDTO
import com.shear.user.dto.OrganizationDTO
import com.shear.user.dto.ProviderDTO
import com.shear.user.dto.UserDTO
import com.shear.user.service.IdpService
import com.shear.user.service.OrganizationService
import com.shear.user.service.ProviderService
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import java.util.*
import javax.inject.Inject

@Controller("/organizations")
class OrganizationController(
    @Inject private val orgService: OrganizationService,
    @Inject private val idpService: IdpService,
    @Inject private val provService : ProviderService
) {

    //TODO: review
    @Post(produces = [MediaType.APPLICATION_JSON])
    fun createOrganization(@Body dto: CreateOrganizationDTO): HttpResponse<OrganizationDTO> {
        val createdUser = idpService.createUser(dto.admin)
        dto.admin = createdUser
        return HttpResponse.created(orgService.createOrganization(dto))
    }

    @Get(value = "/{organizationId}", produces = [MediaType.APPLICATION_JSON])
    fun getOrganization(organizationId: UUID) : HttpResponse<OrganizationDTO?> {
        val org = orgService.getOrganization(organizationId) ?: return HttpResponse.noContent()
        return HttpResponse.ok(org)
    }

    @Get(value = "/{organizationId}/providers", produces = [MediaType.APPLICATION_JSON])
    fun getProvidersUnderOrganization(organizationId: UUID): HttpResponse<List<ProviderDTO?>> {

        val providers = provService.getOrganizationProviders(organizationId)

        if(providers.isNullOrEmpty())
            return HttpResponse.noContent()

        return HttpResponse.ok(providers)
    }


    @Post("/{organizationId}/providers", produces = [MediaType.APPLICATION_JSON])
    fun createOrgProvider(organizationId: UUID, @Body dto: UserDTO): HttpResponse<ProviderDTO?> {
        val createdUser = idpService.createUser(dto)
        createdUser.organizationId = organizationId

        return HttpResponse.created(provService.createProvider(createdUser))
    }


}