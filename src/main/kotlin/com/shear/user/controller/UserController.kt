package com.shear.user.controller

import com.shear.user.dto.UserDTO
import com.shear.user.service.UserService
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import javax.inject.Inject

@Controller("/users")
class UserController(
    @Inject private val service : UserService
) {

    @Post
    fun createUser(@Body dto : UserDTO) = service.createClient(dto)

}