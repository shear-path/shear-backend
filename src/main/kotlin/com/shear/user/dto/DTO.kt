package com.shear.user.dto

import com.shear.user.entity.Location
import com.shear.user.entity.Service
import java.util.*

data class ProviderDTO(
    val id: UUID,
    val firstName: String,
    val lastName: String,
    var email: String?,
    var services: MutableList<Service>?,
    var categories: MutableList<String>?,
    var images: MutableList<String>?,
    var organizationId: UUID?
)

data class UpdateProviderDTO(
    val firstName: String,
    val lastName: String,
    var services: MutableList<Service>?,
    var categories: MutableList<String>?
)

data class CreateOrganizationDTO(
    val name: String,
    val locations: MutableList<Location>,
    var admin : UserDTO
)
data class OrganizationDTO(
    var id: UUID?,
    val name: String,
    val locations: MutableList<Location>,
    var admin: UserDTO?
)

data class UserDTO(
    val firstName: String,
    val lastName: String,
    val email: String,
    val username: String,
    var id: UUID?,
    var organizationId: UUID?
)

