package com.shear.user.dto

import com.shear.user.entity.Admin
import com.shear.user.entity.Client
import com.shear.user.entity.Organization
import com.shear.user.entity.Provider
import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.mapstruct.NullValuePropertyMappingStrategy

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, componentModel = "jsr330")
interface Mapper {

    fun map(providerDTO: ProviderDTO) : Provider

    fun map(updateProviderDTO: UpdateProviderDTO) : Provider

    fun map(userDTO: UserDTO) : ProviderDTO

    fun mapAdmin(adminDTO : UserDTO) : Admin

    fun mapClient(dto : UserDTO) : Client

    fun map(entity : Client) : UserDTO

    fun map(entity : Admin) : UserDTO

    fun map(provider: Provider?) : ProviderDTO?

    fun map(organizationDTO: OrganizationDTO) : Organization

    fun map(dto : CreateOrganizationDTO) : Organization

    fun map(organization: Organization?) : OrganizationDTO

    fun map(dto : UpdateProviderDTO, @MappingTarget provider: Provider?)
}