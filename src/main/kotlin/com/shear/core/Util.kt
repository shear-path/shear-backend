package com.shear.core

import software.amazon.awssdk.enhanced.dynamodb.Key

fun getKey(partitionKey : String, sortKey : String) : Key{
    return Key.builder()
        .partitionValue(partitionKey)
        .sortValue(sortKey)
        .build()
}