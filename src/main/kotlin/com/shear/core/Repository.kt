package com.shear.core

import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbIndex
import software.amazon.awssdk.enhanced.dynamodb.Key
import software.amazon.awssdk.enhanced.dynamodb.TableSchema
import software.amazon.awssdk.enhanced.dynamodb.model.BatchWriteItemEnhancedRequest
import software.amazon.awssdk.enhanced.dynamodb.model.Page
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest
import software.amazon.awssdk.enhanced.dynamodb.model.WriteBatch
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient

/**
 * Interface to provide convenience methods for using the DynamoDbEnhacedClient
 */
interface DynamodbRepository<T> {
    /**
     *  saves an item to the table
     *  @param item The item to be saved in the table
     *  @return The item saved in the table
     */
    fun saveItem(item: T): T

    /**
     * Gets an item in the table
     * @param key The key that identifies the item
     * @return The item identified by the key
     */
    fun getItem(key: Key): T?

    /**
     * Deletes an item from the table
     * @param key The key that identifies the item
     */
    fun deleteItem(key: Key)

    /**
     * Updates an item in the table
     *
     * @param key The key that identifies the item
     * @param item The updated item
     */
    fun updateItem(key: Key, item: T) : T?

    /**
     * Gets an Iterable of items
     *
     * @param query The query to perform on the table
     * @return returns an Iterable of Pages with items
     */
    fun getItems(query: QueryEnhancedRequest): Iterable<Page<T>>

    /**
     * Gets an Iterable of items by querying an index
     *
     * @param index The index to be searched on
     * @param query The query to perform
     * @return returns an Iterable of Pages with items
     */
    fun getItems(index: DynamoDbIndex<T>, query: QueryEnhancedRequest): Iterable<Page<T>>


}

abstract class AbstractDynamodbRepository<T>(
    val tableName: String,
    private val klass: Class<T>,
    region: Region = Region.US_EAST_2,
    client: DynamoDbClient = DynamoDbClient.builder()
        .region(region)
        .build()
) : DynamodbRepository<T> {


    private val enhancedClient = DynamoDbEnhancedClient.builder()
        .dynamoDbClient(client)
        .build()

    val mappedTable = enhancedClient.table(tableName, TableSchema.fromBean(klass))



    override fun getItem(key: Key): T? {
        return mappedTable.getItem(key)
    }

    override fun saveItem(item: T): T {
        val batchWriteItemEnhancedRequest =
            BatchWriteItemEnhancedRequest.builder()
                .writeBatches(
                    WriteBatch.builder(klass)
                        .mappedTableResource(mappedTable)
                        .addPutItem { r -> r.item(item) }
                        .build())
                .build()

        enhancedClient.batchWriteItem(batchWriteItemEnhancedRequest)
        return item
    }

    override fun deleteItem(key: Key) {
        mappedTable.deleteItem(key)
    }

    override fun updateItem(key: Key, item: T) : T? {

        //do nothing if item does not exist
        //DynamodbEnhancedMapper will insert new item with update if it doesn't exist
        val current = getItem(key) ?: return null

        return mappedTable.updateItem(item)
    }

    override fun getItems(query: QueryEnhancedRequest): Iterable<Page<T>> {
        return mappedTable.query(query)
    }

    override fun getItems(index: DynamoDbIndex<T>, query: QueryEnhancedRequest): Iterable<Page<T>> {
        return index.query(query)
    }
}