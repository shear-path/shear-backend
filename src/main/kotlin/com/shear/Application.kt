package com.shear

import io.micronaut.runtime.Micronaut.*
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.info.License

@OpenAPIDefinition(
	info = Info(
		title = "Shear",
		version = "0.0.1.Alpha",
		description = "Shear Backend API",
		contact = Contact( name = "Paul", email = "kangere721@gmail.com")
	)
)
object Application {

	@JvmStatic
	fun main(args: Array<String>) {
		build()
			.args(*args)
			.packages("com.shear")
			.start()
	}

}

